<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Album;
use AppBundle\Service\ImageService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class PageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Route("/albums", name="albums")
     */
    public function albumsAction()
    {
        /** @var ImageService $imageService */
        $imageService = $this->get('app.service.image');

        return new JsonResponse([
            'data' => $imageService->getAlbumListWithImages(),
        ]);
    }

    /**
     * @Route("/images/{albumId}/{page}", name="album_images")
     * @ParamConverter("album", class="AppBundle:Album", options={"id" = "albumId"})
     * @param Album $album
     * @param int $page
     * @return JsonResponse
     */
    public function albumImagesAction(Album $album, $page = 1)
    {
        /** @var ImageService $imageService */
        $imageService = $this->get('app.service.image');

        return new JsonResponse([
            'data' => $imageService->getImagesListForAlbumByPage($album, $page),
        ]);
    }
}
