<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;


/**
 * Class LoadAlbumsAndImages
 * @package AppBundle\DataFixtures\ORM
 */
class LoadAlbumsAndImages implements FixtureInterface
{
    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 5; $i++) {
            $album = new Album();
            $album->setTitle('Album #' . $i);

            $manager->persist($album);

            $max = ($i === 0) ? 5 : rand(21, 50);

            for ($n = 0; $n < $max; $n++) {
                $image = new Image();
                $image->setTitle('Album #' . $i . ', Image #' . $n);
                $image->setAlbum($album);

                $manager->persist($image);
            }
        }
        $manager->flush();
    }
}
