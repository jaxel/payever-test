<?php
namespace AppBundle\Serializer;

use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

/**
 * Class PaginatorNormalizer
 * @package AppBundle\Serializer
 */
class PaginatorNormalizer implements NormalizerInterface
{
    /**
     * @inheritdoc
     */
    public function supportsNormalization($data, $format = null)
    {
        return $data instanceof SlidingPagination;
    }

    /**
     * @param SlidingPagination $object
     * @param null $format
     * @param array $context
     * @return array
     */
    public function normalize($object, $format = null, array $context = [])
    {
        return [
            'currentPageNumber' => (int) $object->getCurrentPageNumber(),
            'itemNumberPerPage' => (int) $object->getItemNumberPerPage(),
            'totalItemCount' => (int) $object->getTotalItemCount(),
            'pageCount' => (int) $object->getPageCount(),
        ];
    }
}