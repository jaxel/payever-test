<?php
namespace AppBundle\Service;

use AppBundle\Entity\Album;
use AppBundle\EntityRepository\AlbumRepository;
use AppBundle\EntityRepository\ImageRepository;
use AppBundle\Serializer\PaginatorNormalizer;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Knp\Component\Pager\Paginator;
use Symfony\Component\Serializer\Serializer;

/**
 * Class ImageService
 * @package AppBundle\Service
 */
class ImageService
{
    /**
     * @var ImageRepository
     */
    protected $imageRepository;

    /**
     * @var AlbumRepository
     */
    protected $albumRepository;

    /** @var Paginator */
    protected $paginator;

    /** @var Serializer */
    protected $serializer;

    /** @var PaginatorNormalizer */
    protected $paginatorNormalizer;

    /** @var integer */
    protected $maxImagesForAlbumList;

    /**
     * @param ImageRepository $imageRepository
     * @param AlbumRepository $albumRepository
     * @param Paginator $paginator
     * @param Serializer $serializer
     * @param PaginatorNormalizer $paginatorNormalizer
     * @param $maxImagesForAlbumList
     */
    public function __construct(
        ImageRepository $imageRepository,
        AlbumRepository $albumRepository,
        Paginator $paginator,
        Serializer $serializer,
        PaginatorNormalizer $paginatorNormalizer,
        $maxImagesForAlbumList
    ) {
        $this->imageRepository = $imageRepository;
        $this->albumRepository = $albumRepository;
        $this->paginator = $paginator;
        $this->serializer = $serializer;
        $this->paginatorNormalizer = $paginatorNormalizer;
        $this->maxImagesForAlbumList = $maxImagesForAlbumList;
    }

    /**
     * @return array
     */
    public function getAlbumListWithImages()
    {
        $albumList = $this->albumRepository->getAlbumListWithImages($this->maxImagesForAlbumList);

        return $this->serializer->normalize($albumList, 'json', ['groups' => ['album']]);
    }

    /**
     * @param Album $album
     * @param int $page
     * @return array
     */
    public function getImagesListForAlbumByPage(Album $album, $page)
    {
        $imageListQuery = $this->imageRepository->getImagesListForAlbumQuery($album);

        /** @var SlidingPagination $paginator */
        $paginator = $this->paginator->paginate($imageListQuery, $page, $this->maxImagesForAlbumList);

        return [
            'paginator' => $this->paginatorNormalizer->normalize($paginator),
            'images' => $this->serializer->normalize($paginator->getItems(), 'json', ['groups' => ['album']])
        ];
    }
}