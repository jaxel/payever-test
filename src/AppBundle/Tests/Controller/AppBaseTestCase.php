<?php

namespace AppBundle\Tests\Controller;

use AppBundle\DataFixtures\ORM\LoadAlbumsAndImages;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Symfony\Bundle\FrameworkBundle\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class AppBaseTestCase
 * @package AppBundle\Tests\Controller
 */
class AppBaseTestCase extends WebTestCase
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Container
     */
    protected $container;

    protected function setUp()
    {
        parent::setUp();

        $this->client = static::createClient([
            'environment' => 'test',
            'debug'       => false,
        ]);

        $this->container = $this->client->getContainer();
    }
}
