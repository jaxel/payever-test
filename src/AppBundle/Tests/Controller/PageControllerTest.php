<?php

namespace AppBundle\Tests\Controller;

/**
 * Class PageControllerTest
 * @package AppBundle\Tests\Controller
 */
class PageControllerTest extends AppBaseTestCase
{
    public function testAlbums()
    {
        $client = static::createClient();

        $client->request('GET', '/');

        $this->assertTrue($client->getResponse()->isSuccessful());

        $client->request('GET', '/albums');

        $this->assertTrue($client->getResponse()->isSuccessful());
    }

    public function testImages()
    {
        $client = static::createClient();

        $client->request('GET', '/images/1/1');

        $this->assertTrue($client->getResponse()->isSuccessful());
    }
}
