<?php
namespace AppBundle\Tests\Service;

use AppBundle\Entity\Album;
use AppBundle\Entity\Image;
use AppBundle\EntityRepository\AlbumRepository;
use AppBundle\EntityRepository\ImageRepository;
use AppBundle\Service\ImageService;
use AppBundle\Tests\Controller\AppBaseTestCase;
use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class ImageServiceTest
 * @package AppBundle\Tests\Service
 */
class ImageServiceTest extends AppBaseTestCase
{
    /**
     *
     */
    public function testGetAlbumListWithImages()
    {
       $imageService = new ImageService(
           $this->getImageRepositoryMock(),
            $this->getAlbumRepositoryMock(),
            $this->container->get('knp_paginator'),
            $this->container->get('serializer'),
            $this->container->get('app.serializer.normalizer.paginator'),
            10
        );

        $albumList = $imageService->getAlbumListWithImages();

        $this->assertEquals(5, count($albumList));

        $this->assertArrayHasKey(0, $albumList);

        $this->assertArrayHasKey('id', $albumList[0]);
        $this->assertArrayHasKey('title', $albumList[0]);
        $this->assertArrayHasKey('images', $albumList[0]);

        $this->assertEquals(5, count($albumList[0]['images']));
        $this->assertEquals(10, count($albumList[1]['images']));

        $this->assertEquals('Album 5, Image 10', $albumList[4]['images'][9]['title']);
    }

    /**
     *
     */
    public function testGetImagesListForAlbumByPage()
    {
        $imageService = new ImageService(
            $this->getImageRepositoryMock(),
            $this->getAlbumRepositoryMock(),
            $this->container->get('knp_paginator'),
            $this->container->get('serializer'),
            $this->container->get('app.serializer.normalizer.paginator'),
            10
        );

        $album = $this->createMock(Album::class);

        $album->expects($this->any())
            ->method('getId')
            ->will($this->returnValue(1));

        $imageList = $imageService->getImagesListForAlbumByPage($album, 1);

        $this->assertArrayHasKey('paginator', $imageList);
        $this->assertArrayHasKey('images', $imageList);

        $this->assertEquals(10, count($imageList['images']));
        $this->assertEquals(4, count($imageList['paginator']));

        $this->assertArrayHasKey('currentPageNumber', $imageList['paginator']);
        $this->assertArrayHasKey('itemNumberPerPage', $imageList['paginator']);
        $this->assertArrayHasKey('totalItemCount', $imageList['paginator']);
        $this->assertArrayHasKey('pageCount', $imageList['paginator']);

        $this->assertEquals(1, $imageList['paginator']['currentPageNumber']);
        $this->assertEquals(10, $imageList['paginator']['itemNumberPerPage']);
        $this->assertEquals(50, $imageList['paginator']['totalItemCount']);
        $this->assertEquals(5, $imageList['paginator']['pageCount']);

        $this->assertEquals('10', $imageList['images'][9]['id']);
        $this->assertEquals('Album 1, Image 10', $imageList['images'][9]['title']);
    }

    /**
     * @return mixed
     */
    protected function getAlbumRepositoryMock()
    {
        for($albumId = 1; 5 >= $albumId; $albumId++) {
            $album = $this->createMock(Album::class);

            $album->expects($this->any())
                ->method('getId')
                ->will($this->returnValue($albumId));
            $album->expects($this->any())
                ->method('getTitle')
                ->will($this->returnValue('Album ' . $albumId));

            $max = ($albumId === 1) ? 5 : 10;

            $imageList = [];

            for($imageId = 1; $max >= $imageId; $imageId++) {
                $image = $this->createMock(Image::class);

                $image->expects($this->any())
                    ->method('getId')
                    ->will($this->returnValue($imageId));
                $image->expects($this->any())
                    ->method('getTitle')
                    ->will($this->returnValue('Album ' . $albumId . ', Image ' . $imageId));

                $imageList[] = $image;
            }

            $album->expects($this->any())
                ->method('getImages')
                ->will($this->returnValue($imageList));

            $albumList[] = $album;
        }

        $albumRepository = $this
            ->getMockBuilder(AlbumRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $albumRepository->expects($this->any())
            ->method('getAlbumListWithImages')
            ->will($this->returnValue($albumList));

        return $albumRepository;
    }

    /**
     * @return mixed
     */
    protected function getImageRepositoryMock()
    {
        $imageList = [];

        for ($imageId = 1; 50 >= $imageId; $imageId++) {
            $image = $this->createMock(Image::class);

            $image->expects($this->any())
                ->method('getId')
                ->will($this->returnValue($imageId));
            $image->expects($this->any())
                ->method('getTitle')
                ->will($this->returnValue('Album 1, Image ' . $imageId));

            $imageList[] = $image;
        }

        $imageRepository = $this
            ->getMockBuilder(ImageRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $imageRepository->expects($this->any())
            ->method('getImagesListForAlbumQuery')
            ->will($this->returnValue($imageList));

        return $imageRepository;
    }
}